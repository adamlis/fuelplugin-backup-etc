# adamlis-backup-etc-attempt1

This plugin will backup /etc directory from nodes.

# Task Details Follow

Onboarding - Fuel Plugin to backup /etc/ to a new backup/NFS server. Cinder NFS plugin.

## Stage 1:

* **DONE** Create a Fuel plugin that will provision a new node with a “backup server” role.
* **DONE** This server should be reachable from other hosts over management network.
* **DONE** It should also have a separate partition dedicated for backups.
* **DONE** rsync package should be installed and configured to work as a daemon.
* **DONE** User should be able to use Fuel UI to specify the name of a folder in which rsync will store data.

## Stage 2:

* **DONE** Expand the plugin to execute a new post-deployment task on all nodes except “backup server” 
* **DONE** which will configure a periodic rsync of /etc/ folder and push it to server with a “backup server” role.

## Stage 3:

* **DONE** Create additional task which will create a backup of /etc/ in pre-deployment stage. This task has to be applied to same hosts as the task defined in previous stage (all hosts but backup server).

## Stage 4:

* **DONE** Expand the plugin to execute a new deployment task on “backup server” nodes which will provide ability to use “backup server” as NFS server. 
* **DONE** User should be able to use Fuel UI to create new NFS shares.

## Stage 5:

* **DONE** Expand plugin so it creates a VIP in public network. 
* **DONE** The new VIP should have all hosts with “backup server” role configured in a backend (using management network).

## Stage 6:

* Create a Fuel plugin that will configure Cinder service to use NFS as backend.
* User should be able to use Fuel UI to choose between using external NFS server or use internal NFS server provided by “backup server” (stage 4). 
* Also user should be able to provide custom mount options for nfs share.

## Advanced: 

* Cinder NFS plugin should be able to configure Cinder service to use multiple NFS shares (using NFS shares from several “backup server” nodes).

# Some issues

## Fuel bug 'text\_list'

Due to a bug in Fuel Plugin Builder if using 'text\_list' patch needs to applied: https://bugs.launchpad.net/fuel/+bug/1616466

