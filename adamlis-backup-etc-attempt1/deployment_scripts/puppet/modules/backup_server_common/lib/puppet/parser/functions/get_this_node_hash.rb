
module Puppet::Parser::Functions

	newfunction(:get_this_node_hash, :type => :rvalue) do |args|

		local_this_node_fqdn = function_hiera(['fqdn'])
		local_all_nodes_hash = function_hiera(['nodes'])

		local_all_nodes_hash.each do |currnode|

			if currnode['fqdn']==local_this_node_fqdn 

				return currnode

			end

		end

		# in case noone of nodes has that exact FQDN - which is not possible
		return "NOT POSSIBLE"

	end

end

