
class backup_server_common::hiera_override (

	# parameters

) inherits backup_server_common {

	# methods

	#
	# hiera override file for plugin
	#
	file { '/etc/hiera/plugins/adamlis-backup-etc-attempt1.yaml':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		content	=> template('backup_server_common/hiera_override.yaml.erb'),
		
	}

}

