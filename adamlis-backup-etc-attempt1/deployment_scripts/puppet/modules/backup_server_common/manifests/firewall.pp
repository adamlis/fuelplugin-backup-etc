
class backup_server_common::firewall (

	# parameters

) inherits backup_server_common {

	$network_scheme = hiera_hash('network_scheme')
	$corosync_networks = get_routable_networks_for_network_role($network_scheme, 'mgmt/corosync')

	$corosync_port = 5405
	
	openstack::firewall::multi_net {'113 corosync-backup-server':
		port		=> $corosync_port,
  		proto		=> 'udp',
		action		=> 'accept',
		source_nets	=> $corosync_networks,
	}

}

