
class backup_server_rsyncd::haproxy-backup_servers-rsyncd (

	# parameters

	$backup_server_vip_public	= $backup_server_rsyncd::backup_server_vip_public,
	$backup_server_vip_mgmt		= $backup_server_rsyncd::backup_server_vip_mgmt,
	$backup_server_nodes_ips	= $backup_server_rsyncd::backup_server_nodes_ips,

) inherits backup_server_rsyncd {

	$backup_server_nodes_ips_keys = keys($backup_server_nodes_ips)
	$backup_server_nodes_ips_values = values($backup_server_nodes_ips)

	notice("(AL) backup_server_nodes_ips_keys: $backup_server_nodes_ips_keys")
	notice("(AL) backup_server_nodes_ips_values: $backup_server_nodes_ips_values")

	openstack::ha::haproxy_service { 'rsync':

		internal_virtual_ip	=> $backup_server_vip_mgmt,
		ipaddresses		=> $backup_server_nodes_ips_values,
		order			=> 410,
		public_virtual_ip	=> $backup_server_vip_public,
		server_names		=> $backup_server_nodes_ips_keys,
		define_backups		=> true,
		internal		=> true,
		public			=> true,
		mode			=> 'tcp',
		listen_port		=> 873,

	}

}

