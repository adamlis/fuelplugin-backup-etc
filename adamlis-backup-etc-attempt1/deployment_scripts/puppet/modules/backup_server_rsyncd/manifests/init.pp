
class backup_server_rsyncd (

	#
	# all values explained in params.pp file
	#

	$plugin_config_all 			= $backup_server_rsyncd::params::plugin_config_all,
	$plugin_rsyncd_password			= $backup_server_rsyncd::params::plugin_rsyncd_password,
	$val_directory_on_backup_server		= $backup_server_rsyncd::params::val_directory_on_backup_server,
	$backup_server_nodes_ips		= $backup_server_rsyncd::params::backup_server_nodes_ips,
	$this_node_fqdn				= $backup_server_rsyncd::params::this_node_fqdn,
	$this_node_ip_mgmt			= $backup_server_rsyncd::params::this_node_ip_mgmt,
	$backup_server_vip_public		= $backup_server_rsyncd::params::backup_server_vip_public,
	$backup_server_vip_mgmt			= $backup_server_rsyncd::params::backup_server_vip_mgmt,
	$management_vip				= $backup_server_rsyncd::params::management_vip,
	$public_vip				= $backup_server_rsyncd::params::public_vip,

) inherits backup_server_rsyncd::params {

}

