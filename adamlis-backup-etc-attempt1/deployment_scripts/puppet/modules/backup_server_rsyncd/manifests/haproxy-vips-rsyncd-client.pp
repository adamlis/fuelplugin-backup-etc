
class backup_server_rsyncd::haproxy-vips-rsyncd-client (

	# parameters

	$management_vip			= $backup_server_rsyncd::management_vip,
	$public_vip			= $backup_server_rsyncd::public_vip,
	$backup_server_vip_public	= $backup_server_rsyncd::backup_server_vip_public,
	$backup_server_vip_mgmt		= $backup_server_rsyncd::backup_server_vip_mgmt,

) inherits backup_server_rsyncd {

	#
	# cronjob configuration for client to backup to VIPs created 
	#
	file { '/etc/cron.d/cronjob-backup-to-backup_server-via-vips':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		content	=> template('backup_server_rsyncd/cronjob-backup-to-backup_server-vips.erb'),

	}


}

