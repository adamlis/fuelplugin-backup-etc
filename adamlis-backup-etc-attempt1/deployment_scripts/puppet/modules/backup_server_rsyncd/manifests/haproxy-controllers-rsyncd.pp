
class backup_server_rsyncd::haproxy-controllers-rsyncd (

	# parameters

	$management_vip			= $backup_server_rsyncd::management_vip,
	$public_vip			= $backup_server_rsyncd::public_vip,
	$backup_server_nodes_ips	= $backup_server_rsyncd::backup_server_nodes_ips,

) inherits backup_server_rsyncd {

	#
	# NOTICE !!!
	#
	# this HAProxy configuration is obsolete in a way - onboarding tasks included
	# creating HAProxy service first on controllers and then on dedicated nodes;
	# this file contains declaration of HAProxy on controllers, another confguration
	# file contains similar configuration using newly created VIPs on newly created
	# corosync/pacemaker instance on nodes with role backup_server
	#

	$backup_server_nodes_ips_keys = keys($backup_server_nodes_ips)
	$backup_server_nodes_ips_values = values($backup_server_nodes_ips)

	notice("(AL) backup_server_nodes_ips_keys: $backup_server_nodes_ips_keys")
	notice("(AL) backup_server_nodes_ips_values: $backup_server_nodes_ips_values")

	openstack::ha::haproxy_service { 'rsync':

		internal_virtual_ip	=> $management_vip,
		ipaddresses		=> $backup_server_nodes_ips_values,
		order			=> 310,
		public_virtual_ip	=> $public_vip,
		server_names		=> $backup_server_nodes_ips_keys,
		define_backups		=> true,
		internal		=> true,
		public			=> true,
		mode			=> 'tcp',
		listen_port		=> 873,

	}

}

