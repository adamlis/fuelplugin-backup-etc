
class backup_server_rsyncd::service_server (

	# parameters

) inherits backup_server_rsyncd {

	# methods

	#
	# ensure service 'rsync' enabled and running
	#
 	service { 'rsync':

		enable	=> true,
		ensure	=> running,

	}

}


