
class backup_server_rsyncd::install_client (

	# parameters

) inherits backup_server_rsyncd {

	# methods

	package { 'rsync':
		ensure => 'installed',
	}

	package { 'cron':
		ensure => 'installed',
	}
 
}

