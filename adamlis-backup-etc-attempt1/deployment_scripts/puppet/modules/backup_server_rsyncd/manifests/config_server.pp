
class backup_server_rsyncd::config_server (

	# parameters

	$val_directory_on_backup_server	= $backup_server_rsyncd::val_directory_on_backup_server,
	$this_node_ip_mgmt		= $backup_server_rsyncd::this_node_ip_mgmt,
	$plugin_rsyncd_password		= $backup_server_rsyncd::plugin_rsyncd_password,

) inherits backup_server_rsyncd {

	# methods

	#
	# main rsync configuration file taken from template
	#
	file { '/etc/rsyncd.conf':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		content	=> template('backup_server_rsyncd/rsyncd.conf.erb'),

	}

	#
	# Debian's default configuration file for rsync
	#
	file { '/etc/default/rsync':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		content	=> template('backup_server_rsyncd/etc_default_rsync.erb'),
	}

	#
	# secrets file for keeping password
	#
	file { '/etc/rsyncd.secrets':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0600',
		content	=> template('backup_server_rsyncd/rsyncd.secrets.erb'),

	}

	#
	# directory relative to /backup specified by user in plugin configuration
	#
	file { "/backup/$val_directory_on_backup_server":

		ensure	=> directory,
		owner	=> 'root',
		group	=> 'root',

	}

	#
	# prepare firewall rule for rsyncd
	#
	firewall {'873 rsync tcp':
		dport   => [ 873 ],
		proto  => 'tcp',
		action => 'accept',
	}


}

