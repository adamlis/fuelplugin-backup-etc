
class backup_server_rsyncd::config_client (

	# parameters
	$backup_server_nodes_ips	= $backup_server_rsyncd::backup_server_nodes_ips,
	$this_node_fqdn			= $backup_server_rsyncd::this_node_fqdn,
	$plugin_rsyncd_password		= $backup_server_rsyncd::plugin_rsyncd_password,

) inherits backup_server_rsyncd {

	# methods

	#
	# cronjob configuration for client to backup to all nodes with role backup_server
	#
	file { '/etc/cron.d/cronjob-backup-to-backup_server-nodes':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		content	=> template('backup_server_rsyncd/cronjob-backup-to-backup_server-nodes.erb'),

	}

	#
	# secrets file for keeping password
	#
	file { '/etc/rsync-client.secrets':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		mode	=> '0600',
		content	=> template('backup_server_rsyncd/rsync-client.secrets.erb'),

	}


}

