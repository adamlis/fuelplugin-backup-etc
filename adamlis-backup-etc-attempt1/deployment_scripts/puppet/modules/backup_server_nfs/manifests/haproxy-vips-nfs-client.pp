
class backup_server_nfs::haproxy-vips-nfs-client (

	# parameters

	$this_node_fqdn			= $backup_server_nfs::this_node_fqdn,
	$val_nfs_shares			= $backup_server_nfs::val_nfs_shares,
	$management_vip			= $backup_server_nfs::management_vip,
	$public_vip			= $backup_server_nfs::public_vip,
	$backup_server_vip_public	= $backup_server_nfs::backup_server_vip_public,
	$backup_server_vip_mgmt		= $backup_server_nfs::backup_server_vip_mgmt,

) inherits backup_server_nfs {

	# methods

	#
	# prepare data structure for directories and shares creation
	#
	$mountpoints_nfs_haproxy = prepare_mountpoints_nfs_haproxy (
		{
		haproxy_server_ips_internal		=> {
					'public-vip'		=> $public_vip,
					'mgmt-vip'		=> $management_vip, 
					'backup-vip-public'	=> $backup_server_vip_public,
					'backup-vip-mgmt'	=> $backup_server_vip_mgmt,
					},

		val_nfs_shares_internal			=> $val_nfs_shares,
		}
	)
	notice("(AL) backup_server_nfs::haproxy-vips-nfs-client -> got mountpoints_nfs_haproxy: $mountpoints_nfs_haproxy")
	
	#
	# get keys from prepared structure
	#
	$mountpoints_nfs_haproxy_keys = keys($mountpoints_nfs_haproxy)
	notice("(AL) backup_server_nfs::haproxy-vips-nfs-client -> extracted mountpoints_nfs_haproxy_keys: $mountpoints_nfs_haproxy_keys")

	#
	# directory relative to /backup for keeping nfs shares plus subdirectories for shares
	#
	file { "/_nfs_mounts_2":

		name	=> "/_nfs_mounts",
		ensure	=> directory,
		owner	=> 'root',
		group	=> 'root',

	}

		->

	backup_server_nfs::resources::create_mountpoints_nfs { $mountpoints_nfs_haproxy_keys:

		values_hash	=> $mountpoints_nfs_haproxy,

	}


}

