
class backup_server_nfs::install_server (

	# parameters

) inherits backup_server_nfs {

	# methods

	package { 'nfs-kernel-server' :
		ensure => 'installed',
	}
 
}

