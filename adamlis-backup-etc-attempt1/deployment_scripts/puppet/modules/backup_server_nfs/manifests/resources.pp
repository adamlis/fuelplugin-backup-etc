
class backup_server_nfs::resources (

	# parameters

) inherits backup_server_nfs {

	#
	# take neccessary actions to create mount to nfs on some server
	#
	define create_mountpoints_nfs (
		$values_hash = {},
	) {

		notice("(AL) backup_server_nfs::config_client -> called 'create_mountpoints_nfs' with name: '$name'")

		$this_values_hash = $values_hash[$name]

		notice("(AL) backup_server_nfs::config_client -> configuration for this share: '$this_values_hash'")

		file { "/_nfs_mounts/$name":

			ensure	=> directory,
			owner	=> 'root',
			group	=> 'root',
		}
		
			->

		mount { "/_nfs_mounts/$name":
	
			ensure	=> present,
			device	=> join([$this_values_hash['node_ip'],":/backup/_nfs_shares/",$this_values_hash['share_name']]),
			fstype	=> 'nfs4',
			options	=> 'tcp,noauto',

		}

	}

}

