
class backup_server_nfs::params {

	#
	# plugin data including all configurable parameters - kept in hiera
	#
	$plugin_data_all = hiera('adamlis-backup-etc-attempt1')

	#
	# user configurable parameter - val_nfs_shares
	#
	$val_nfs_shares = $plugin_data_all['adamlis-backup-etc-attempt1_nfs_shares']
	notice("(AL) backup_server_nfs::params -> val_nfs_shares: $val_nfs_shares")

	#
	# network_metadata
	#
	$network_metadata = hiera_hash('network_metadata')

	#
	# backup_server nodes list - usable mostly by client side
	#
	$backup_server_nodes_hash = get_nodes_hash_by_roles($network_metadata, ['primary-backup_server', 'backup_server'])
	notice("(AL) backup_server_nfs::params -> backup_server_nodes_hash: $backup_server_nodes_hash")

	#
	# backup_server_nodes_ips - list of IPs of nodes with role 'backup_server' on interface 'mgmt/backup_server'
	#
	$backup_server_nodes_ips = get_node_to_ipaddr_map_by_network_role($backup_server_nodes_hash,'mgmt/backup_server')
	notice("(AL) backup_server_nfs::params -> backup_server_nodes_ips: $backup_server_nodes_ips")

	#
	# this_node_fqdn - represents current node FQDN
	#
	$this_node_fqdn = hiera('fqdn')
	notice("(AL) backup_server_nfs::params -> this_node_fqdn: $this_node_fqdn")

	#
	# this_node_hash - return entry from Hiera 'node' array for this node
	#
	$this_node_hash = get_this_node_hash()
	notice("(AL) backup_server_nfs::params -> this_node_hash: $this_node_hash")

	#
	# this_node_ip_mgmt - return IP address of this node in network 'mgmt/backup_server' - only on nodes with role 'backup_server'
	#
	$this_node_ip_mgmt = $backup_server_nodes_ips[$this_node_hash['name']]
	notice("(AL) backup_server_nfs::params (empty on nodes without role backup_server) -> this_node_ip_mgmt: $this_node_ip_mgmt")

	#
	# backup_server_vip_public - return IP address of VIP created in public network
	#
	$backup_server_vip_public = $network_metadata['vips']['backup_server_vip_public']['ipaddr']
	notice("(AL) backup_server_nfs::params -> backup_server_vip_public: $backup_server_vip_public")

	#
	# backup_server_vip_mgmt - return IP address of VIP created in management network
	#
	$backup_server_vip_mgmt = $network_metadata['vips']['backup_server_vip_mgmt']['ipaddr']
	notice("(AL) backup_server_nfs::params -> backup_server_vip_mgmt: $backup_server_vip_mgmt")

	#
	# management_vip - return IP address of general management VIP address
	#
	$management_vip = hiera('management_vip')
	notice("(AL) backup_server_nfs::params -> management_vip: $management_vip")

	#
	# public_vip - return IP address of general public VIP address
	#
	$public_vip = hiera('public_vip')
	notice("(AL) backup_server_nfs::params -> public_vip: $public_vip")


}

