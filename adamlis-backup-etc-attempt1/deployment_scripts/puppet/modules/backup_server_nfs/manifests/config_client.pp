
class backup_server_nfs::config_client (

	# parameters
	$backup_server_nodes_ips	= $backup_server_nfs::backup_server_nodes_ips,
	$this_node_fqdn			= $backup_server_nfs::this_node_fqdn,
	$val_nfs_shares			= $backup_server_nfs::val_nfs_shares,

) inherits backup_server_nfs {

	# methods

	#
	# prepare data structure for directories and shares creation
	#
	$mountpoints_nfs_direct = prepare_mountpoints_nfs_direct (
		{
		backup_server_nodes_ips_internal	=> $backup_server_nodes_ips,
		val_nfs_shares_internal			=> $val_nfs_shares,
		}
	)
	notice("(AL) backup_server_nfs::config_client -> got mountpoints_nfs_direct: $mountpoints_nfs_direct")
	
	#
	# get keys from prepared structure
	#
	$mountpoints_nfs_direct_keys = keys($mountpoints_nfs_direct)
	notice("(AL) backup_server_nfs::config_client -> extracted mountpoints_nfs_direct_keys: $mountpoints_nfs_direct_keys")

	#
	# directory relative to /backup for keeping nfs shares plus subdirectories for shares
	#
	file { "/_nfs_mounts":

		ensure	=> directory,
		owner	=> 'root',
		group	=> 'root',

	}

		->

	backup_server_nfs::resources::create_mountpoints_nfs { $mountpoints_nfs_direct_keys:

		values_hash	=> $mountpoints_nfs_direct,

	}

}

