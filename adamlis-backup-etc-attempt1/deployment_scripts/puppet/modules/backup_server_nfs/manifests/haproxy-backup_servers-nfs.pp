
class backup_server_nfs::haproxy-backup_servers-nfs (

	# parameters

	$backup_server_vip_public	= $backup_server_nfs::backup_server_vip_public,
	$backup_server_vip_mgmt		= $backup_server_nfs::backup_server_vip_mgmt,
	$backup_server_nodes_ips	= $backup_server_nfs::backup_server_nodes_ips,

) inherits backup_server_nfs {

	$backup_server_nodes_ips_keys = keys($backup_server_nodes_ips)
	$backup_server_nodes_ips_values = values($backup_server_nodes_ips)

	notice("(AL) backup_server_nodes_ips_keys: $backup_server_nodes_ips_keys")
	notice("(AL) backup_server_nodes_ips_values: $backup_server_nodes_ips_values")

	openstack::ha::haproxy_service { 'nfs':

		internal_virtual_ip	=> $backup_server_vip_mgmt,
		ipaddresses		=> $backup_server_nodes_ips_values,
		order			=> 420,
		public_virtual_ip	=> $backup_server_vip_public,
		server_names		=> $backup_server_nodes_ips_keys,
		define_backups		=> true,
		internal		=> true,
		public			=> true,
		mode			=> 'tcp',
		listen_port		=> 2049,

	}

}

