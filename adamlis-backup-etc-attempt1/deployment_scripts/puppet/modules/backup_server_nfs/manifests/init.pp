
class backup_server_nfs (

	$plugin_config_all 			= $backup_server_nfs::params::plugin_config_all,
	$val_nfs_shares				= $backup_server_nfs::params::val_nfs_shares,
	$backup_server_nodes_ips		= $backup_server_nfs::params::backup_server_nodes_ips,
	$this_node_fqdn				= $backup_server_nfs::params::this_node_fqdn,
	$this_node_ip_mgmt			= $backup_server_nfs::params::this_node_ip_mgmt,
	$backup_server_vip_public		= $backup_server_nfs::params::backup_server_vip_public,
	$backup_server_vip_mgmt			= $backup_server_nfs::params::backup_server_vip_mgmt,
	$management_vip				= $backup_server_nfs::params::management_vip,
	$public_vip				= $backup_server_nfs::params::public_vip,

) inherits backup_server_nfs::params {

}

