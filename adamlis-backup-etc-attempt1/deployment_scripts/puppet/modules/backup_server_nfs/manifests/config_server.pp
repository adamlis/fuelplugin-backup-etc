
class backup_server_nfs::config_server (

	# parameters

	$val_nfs_shares			= $backup_server_nfs::val_nfs_shares,
	$this_node_ip_mgmt		= $backup_server_nfs::this_node_ip_mgmt,

) inherits backup_server_nfs {

	# methods

	#
	# create subdirectories with names taken from array
	#
	define create_nfs_subdirs {

		notice("(AL) backup_server_nfs::config_server => found nfs share: $name")

		file { "/backup/_nfs_shares/$name":

			ensure	=> directory,
			owner	=> 'nobody',
			group	=> 'nogroup',

		}
	}


	#
	# directory relative to /backup for keeping nfs shares plus subdirectories for shares
	#
	file { "/backup/_nfs_shares":

		ensure	=> directory,
		owner	=> 'root',
		group	=> 'root',

	}
		->
	create_nfs_subdirs { $val_nfs_shares: }

	#
	# cronjob configuration for client to backup to all nodes with role backup_server
	#
	file { '/etc/exports':

		ensure	=> file,
		owner	=> 'root',
		group	=> 'root',
		content	=> template('backup_server_nfs/nfs_exports.erb'),

	}


	#
	# prepare firewall rule for nfs
	#
	firewall {'2049 nfs tcp':
		dport   => [ 2049 ],
		proto  => 'tcp',
		action => 'accept',
	}


}

