
class backup_server_nfs::service_server (

	# parameters

) inherits backup_server_nfs {

	# methods

	#
	# ensure service 'nfs-kernel-server' enabled and running
	#
 	service { 'nfs-kernel-server':

		enable	=> true,
		ensure	=> running,

	}

}


