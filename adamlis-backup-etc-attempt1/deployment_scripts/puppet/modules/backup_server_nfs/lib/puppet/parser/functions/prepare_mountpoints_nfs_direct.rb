
module Puppet::Parser::Functions

	newfunction(:prepare_mountpoints_nfs_direct, :type => :rvalue) do |args|

		notice("(AL) called prepare_mountpoints_nfs_direct")

		$local_backup_server_nodes_ips_internal	= args[0]['backup_server_nodes_ips_internal']
		$local_val_nfs_shares_internal = args[0]['val_nfs_shares_internal']

		$retvalue={}

		$local_val_nfs_shares_internal.each do |share_name|

			$local_backup_server_nodes_ips_internal.each do |node_name,node_ip|

				$tmp_resname=share_name+"_via_node_"+node_name
				$tmp_value={
					'node_name'	=> node_name,
					'node_ip'	=> node_ip,
					'share_name'	=> share_name,
				}

				$retvalue[$tmp_resname]=$tmp_value

			end

		end

		return $retvalue

	end

end

