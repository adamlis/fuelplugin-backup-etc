
module Puppet::Parser::Functions

	newfunction(:prepare_mountpoints_nfs_haproxy, :type => :rvalue) do |args|

		notice("(AL) called prepare_mountpoints_nfs_haproxy")

		$local_haproxy_server_ips_internal = args[0]['haproxy_server_ips_internal']
		$local_val_nfs_shares_internal = args[0]['val_nfs_shares_internal']

		$retvalue={}

		$local_val_nfs_shares_internal.each do |share_name|

			$local_haproxy_server_ips_internal.each do |endpoint_type,node_ip|

				$tmp_resname=share_name+"_via_"+endpoint_type
				$tmp_value={
					'node_ip'	=> node_ip,
					'share_name'	=> share_name,
				}

				$retvalue[$tmp_resname]=$tmp_value

			end

		end

		return $retvalue

	end

end

