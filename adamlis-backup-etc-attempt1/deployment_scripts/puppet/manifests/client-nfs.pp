
notice('(AL) Running manifest: client-nfs.pp')

include  backup_server_nfs

#
# 2 steps to define NFS mount-points: install_client and config_client 
#

class { 'backup_server_nfs::install_client':

}

->

class { 'backup_server_nfs::config_client': 

}

