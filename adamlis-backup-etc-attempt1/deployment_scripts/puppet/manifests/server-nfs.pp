
notice('(AL) Running manifest: install-nfs.pp')

include backup_server_nfs

#
# 3 steps to run rsync: install_server, config_server and service_server
#

class { 'backup_server_nfs::install_server':

}

->

class { 'backup_server_nfs::config_server':

}

->

class { 'backup_server_nfs::service_server':

}

