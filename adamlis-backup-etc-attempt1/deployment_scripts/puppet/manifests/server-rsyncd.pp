
notice('(AL) Running manifest: install-rsyncd.pp')

include backup_server_rsyncd

#
# 3 steps to run rsync: install_server, config_server and service_server
#

class { 'backup_server_rsyncd::install_server':

}

->

class { 'backup_server_rsyncd::config_server': 

}

->

class { 'backup_server_rsyncd::service_server':

}

