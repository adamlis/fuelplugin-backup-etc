
notice('(AL) Running manifest: client-rsyncd.pp')

include  backup_server_rsyncd

#
# 3 steps to run client cronjob: install_client, config_client and service_client
#

class { 'backup_server_rsyncd::install_client':

}

->

class { 'backup_server_rsyncd::config_client': 

}

->

class { 'backup_server_rsyncd::service_client': 

}

